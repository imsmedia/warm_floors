<?php
session_start();
$site_url = 'http://stk-floors.imsmedia.in.ua/';
if (array_key_exists('utm_referrer', $_GET)) {
  if (strpos($site_url, $_GET['utm_referrer'])===false) {
    $_SESSION['referer'] = $_GET['utm_referrer'];
  }
}
if (array_key_exists('utm_source', $_GET)) {
  if (strpos($site_url, $_GET['utm_source'])===false) {
    $_SESSION['sourse'] = $_GET['utm_source'];
  }
  if (strpos($site_url, $_GET['utm_term'])===false) {
    $_SESSION['term'] = $_GET['utm_term'];
  }
  if (strpos($site_url, $_GET['utm_campaign'])===false) {
    $_SESSION['campaign'] = $_GET['utm_campaign'];
  }
}
?>
<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="UTF-8">
  <title>Теплый пол STK</title>
  <meta name="description" content="Теплый пол STK">
  <meta name='viewport' content='width=1200, initial-scale=1'>
  <link rel="apple-touch-icon" sizes="57x57" href="/img/favicons/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="/img/favicons/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="/img/favicons/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="/img/favicons/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="/img/favicons/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="/img/favicons/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="/img/favicons/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="/img/favicons/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="/img/favicons/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="/img/favicons/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/img/favicons/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="/img/favicons/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/img/favicons/favicon-16x16.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/img/favicons/favicon-16x16.png">
  <link rel="shortcut icon" href="/img/favicons/favicon.ico" type="image/x-icon">
  <link rel="manifest" href="/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/img/favicons/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <link rel="stylesheet" href="/js/libs/fancybox/jquery.fancybox.css">
  <link rel="stylesheet" href="/css/index.css">
  <script src="/js/libs/jquery.min.js"></script>
  <script src="/js/libs/slick.min.js"></script>
  <script src="/js/libs/maskedinput.min.js"></script>
  <script src="/js/libs/jquery.validate.min.js"></script>
  <script src="/js/libs/tabs.js"></script>
  <script src="/js/libs/fancybox/jquery.fancybox.pack.js" charset="utf-8"></script>
</head>

<body>
  <div class="wrapper bg-wrapper-1 noise">
    <header class="header">
      <div class="header__contacts">
        <span class="header__phone">+38 (097) 856-65-66</span>
        <button class="header__callback-btn" data-init="modal" data-modal="#callback-modal" data-title="ЗАКАЗАТЬ ОБРАТНЫЙ ЗВОНОК"  data-order="Заказ обратного звонка" data-hidden="/callback-header.html">Обратный звонок</button>
      </div>
      <div class="header__logo"></div>
    </header>
    <div class="inner">
      <div class="scr-i">
        <div class="scr-i__image-wrapper"><img src="/img/index/hose.png" alt="" class="scr-i__image"></div>
        <span class="scr-i__title">Сделайте Ваш дом теплее<br>уже через 3 дня при помощи<br>современных труб теплого пола<br>с доставкой!</span>
        <form action="sendmessage.php" class="scr-i-form">
          <div class="scr-i-form__col--gift">
            <span class="scr-i-form__gift-title">
              в ПОДАРОК<br>манометр
            </span>
            <img class="scr-i-form__gift-image" src="/img/index/monometer.png" alt="">
          </div>
          <div class="scr-i-form__col--right">
            <span class="scr-i-form__title">
              Оставьте заявку и получите подарок
            </span>
            <div class="scr-i-form__input-wrapper">
              <input class="scr-i-form__input" type="text" name="phone" placeholder="+38 (0__) ___-__-__">
            </div>
            <input type="hidden" name="order" value="Первый экран - получить подарок">
            <input type="hidden" name="tagmanager" value="/first-screen-gift.html">
            <button id="gift-button" type="submit" class="scr-i-form__btn">ПОЛУЧИТЬ ПОДАРОК</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="wrapper noise scr-ii__wrapper">
    <div class="inner scr-ii">
      <span class="scr-ii__title">преимущества теплых полов stk</span>
      <div class="advantages">
        <div class="advantages__row">
          <div class="advantage">
            <span class="advantage__title">Монтаж без сварки</span>
            <span class="advantage__description">Соединение при помощи аксессуаров и фитингов</span>
          </div>
          <div class="advantage">
            <span class="advantage__title">Эластичность и гибкость</span>
            <span class="advantage__description">Труба легко гнется в холодном режиме без доп оборудования</span>
          </div>
          <div class="advantage">
            <span class="advantage__title">Термоустойчивость</span>
            <span class="advantage__description">Работает при температурах до 110 градусов</span>
          </div>
        </div>
        <div class="advantages__row--two-items">
          <div class="advantage">
            <span class="advantage__title">Долговечность</span>
            <span class="advantage__description">Соединение при помощи аксессуаров и фитингов</span>
          </div>
          <div class="advantage">
            <span class="advantage__title">Устойчивость к давлению</span>
            <span class="advantage__description">Выдерживает давление до 25 атмосфер</span>
          </div>
        </div>
        <div class="advantages__row">
          <div class="advantage">
            <span class="advantage__title">Устойчивость к коррозии</span>
            <span class="advantage__description">Пригодна для использования с коррозийными жидкостями</span>
          </div>
          <div class="advantage">
            <span class="advantage__title">Экологичность</span>
            <span class="advantage__description">Не меняет характеристик питьевой воды</span>
          </div>
          <div class="advantage">
            <span class="advantage__title">Бесшумность</span>
            <span class="advantage__description">На 120% тише чем железная труба</span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="wrapper scr-iii__wrapper">
    <div class="inner scr-iii">
      <span class="scr-iii__title">ВАРИАНТЫ КОМПЛЕКТАЦИИ ПОСТАВКИ</span>
      <div id="tabs" class="product-tabs">
        <div class="tabs-nav">
          <ul>
            <li id="premium-tab"><span>premium</span></li>
            <li id="comfort-tab"><span>comfort</span></li>
          </ul>
        </div>
        <div class="tabs-content">
          <ul>
            <li>
              <div class="tab__left">
                <img src="/img/index/tab-1.png" alt="">
                <span>ПОДАРОК</span>
              </div>
              <div class="tab__right">
                <span class="tab__old-price">
                  <span class="number">1001</span><span class="currency"> грн</span>
                </span>
                <span class="tab__new-price">
                  <span class="number">888</span><span class="currency"> грн</span>
                </span>
                <span class="tab__price-description">за метр погонный</span>
                <form class="tab__form" action="sendmessage.php" method="post">
                  <div class="tab__input-wrapper">
                    <input type="text" name="phone" placeholder="+38 (0__) ___-__-__" class="tab__input">
                    <input type="hidden" name="order" value="PREMIUM - купить со скидкой">
                  </div>
                  <div class="tab__input-wrapper">
                    <button id="premium-button" class="tab__btn" type="submit">купить со скидкой</button>
                  </div>
                  <input type="hidden" name="tagmanager" value="/buy-premium.html">
                </form>
                <div class="tab__prod-info">
                  <span class="tab__prod-name">
                    ТРУБА GOLG-PEX
                  </span>
                  <div class="tab__prod-characters">
                    <span>Наружный диаметр — 16.0 mm</span>
                    <span>Внутренний диаметр — 12.0 mm </span>
                    <span>Толщина стенки трубы — 2.0 mm </span>
                    <span>Кислородный барьер —  </span>
                    <span>Вид сшитого полиэтилена — Пероксидный (PE-Xa) </span>
                    <span>Степень сшивки — 80-85% </span>
                    <span>Молекулярная связь — С-С </span>
                    <span>Труба способна проработать заявленные 50 лет в режиме 90С/7 бар или 70С/11 бар</span>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div class="tab__left">
                <img src="/img/index/tab-2.png" alt="">
              </div>
              <div class="tab__right">
                <span class="tab__new-price--red">
                  <span class="number">777</span><span class="currency"> грн</span>
                </span>
                <span class="tab__price-description">за метр погонный</span>
                <form class="tab__form" action="sendmessage.php" method="post">
                  <div class="tab__input-wrapper">
                    <input type="text" name="phone" placeholder="+38 (0__) ___-__-__" class="tab__input--red">
                    <input type="hidden" name="order" value="COMFORT - купить со скидкой">
                  </div>
                  <div class="tab__input-wrapper">
                    <button id="comfort-button" class="tab__btn--red" type="submit">купить со скидкой</button>
                  </div>
                  <input type="hidden" name="tagmanager" value="/buy-comfort.html">
                </form>
                <div class="tab__prod-info--red">
                  <span class="tab__prod-name--red">
                  ТРУБА RED-PEX
                </span>
                  <div class="tab__prod-characters">
                    <span>Наружный диаметр — 16.0 mm</span>
                    <span>Внутренний диаметр — 12.0 mm </span>
                    <span>Толщина стенки трубы — 2.0 mm </span>
                    <span>Кислородный барьер —  </span>
                    <span>Вид сшитого полиэтилена — Пероксидный (PE-Xa) </span>
                    <span>Степень сшивки — 80-85% </span>
                    <span>Молекулярная связь — С-С  </span>
                    <span>Труба способна проработать заявленные 50 лет в режиме 90С/7 бар или 70С/11 бар</span>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="wrapper scr-iv__wrapper bg-wrapper-2">
    <div class="inner scr-iv">
      <span class="scr-iv__title">
        Доставка в любую точку<br>Украины за 2 дня
      </span>
      <div class="scr-iv__new-post-logo"></div>
    </div>
  </div>
  <div class="wrapper scr-v__wrapper">
    <div class="inner scr-v">
      <span class="scr-v__title">почему уже 1024 клиентов выбрали наш теплый пол</span>
      <div class="why">
        <div class="why__item--1">
          <span class="why__item-number">1</span>
          <span class="why__item-title">MADE<br>FOR EU</span>
        </div>
        <div class="why__item--2">
          <span class="why__item-number">2</span>
          <span class="why__item-title">ГАРАНТИЯ<br>20 ЛЕТ</span>
        </div>
        <div class="why__item--3">
          <span class="why__item-number">3</span>
          <span class="why__item-title">БЫСТРАЯ<br>ДОСТАВКА</span>
        </div>
        <div class="why__item--4">
          <span class="why__item-number">4</span>
          <span class="why__item-title">ОПЛАТА<br>ПОСЛЕ<br>ПОЛУЧЕНИЯ</span>
        </div>
      </div>
    </div>
  </div>
  <div class="wrapper scr-vi__wrapper">
    <div class="inner scr-vi">
      <span class="scr-vi__title">РАСШИРЯЕМ ДИЛЕРСКУЮ СЕТЬ</span>
      <button class="scr-vi__btn" type="button"  data-init="modal" data-modal="#callback-modal" data-title="получить предложение для партнера" data-order="Заказ специального предложения" data-hidden="/partners-button.html">Специальные предложения для партнеров</button>
    </div>
  </div>
  <div class="scr-vii__wrapper wrapper">
    <div class="inner scr-vii">
      <div class="reviews" id="reviews">
        <div class="review">
          <div class="review__main-image-wrapper">
            <img src="/img/index/review-face-1.jpg" alt="">
          </div>
          <div class="review__content">
            <span class="review__name">
              МЕТТ бОМЕР
            </span>
            <p class="review__text">
              Пятый сезон антологии «Американская история ужасов» заполучил новых исполнителей главных ролей. Ими стали Мэтт Бомер и Шайен Джексон. Бомер уже принимал участие в съемках одного из эпизодов «Американской истории ужасов». К тому же это будет продолжение довольно успешного сотрудничества Мэтта.
            </p>
          </div>
          <div class="review__images">
            <a class="fancybox" rel="gallery-1" href="/img/index/review-image-big-1-1.jpg"><img src="/img/index/review-image-1-1.jpg" alt=""></a>
            <a class="fancybox" rel="gallery-1" href="/img/index/review-image-big-1-1.jpg"><img src="/img/index/review-image-1-1.jpg" alt=""></a>
            <a class="fancybox" rel="gallery-1" href="/img/index/review-image-big-1-1.jpg"><img src="/img/index/review-image-1-1.jpg" alt=""></a>
          </div>
        </div>
        <div class="review">
          <div class="review__main-image-wrapper">
            <img src="/img/index/review-face-1.jpg" alt="">
          </div>
          <div class="review__content">
            <span class="review__name">
              МЕТТ бОМЕР
            </span>
            <p class="review__text">
              Пятый сезон антологии «Американская история ужасов» заполучил новых исполнителей главных ролей. Ими стали Мэтт Бомер и Шайен Джексон. Бомер уже принимал участие в съемках одного из эпизодов «Американской истории ужасов». К тому же это будет продолжение довольно успешного сотрудничества Мэтта.
            </p>
          </div>
          <div class="review__images">
            <a class="fancybox" rel="gallery-2" href="/img/index/review-image-big-1-1.jpg"><img src="/img/index/review-image-1-1.jpg" alt=""></a>
            <a class="fancybox" rel="gallery-2" href="/img/index/review-image-big-1-1.jpg"><img src="/img/index/review-image-1-1.jpg" alt=""></a>
            <a class="fancybox" rel="gallery-2" href="/img/index/review-image-big-1-1.jpg"><img src="/img/index/review-image-1-1.jpg" alt=""></a>
          </div>
        </div>
        <div class="review">
          <div class="review__main-image-wrapper">
            <img src="/img/index/review-face-1.jpg" alt="">
          </div>
          <div class="review__content">
            <span class="review__name">
              МЕТТ бОМЕР
            </span>
            <p class="review__text">
              Пятый сезон антологии «Американская история ужасов» заполучил новых исполнителей главных ролей. Ими стали Мэтт Бомер и Шайен Джексон. Бомер уже принимал участие в съемках одного из эпизодов «Американской истории ужасов». К тому же это будет продолжение довольно успешного сотрудничества Мэтта.
            </p>
          </div>
          <div class="review__images">
            <a class="fancybox" rel="gallery-3" href="/img/index/review-image-big-1-1.jpg"><img src="/img/index/review-image-1-1.jpg" alt=""></a>
            <a class="fancybox" rel="gallery-3" href="/img/index/review-image-big-1-1.jpg"><img src="/img/index/review-image-1-1.jpg" alt=""></a>
            <a class="fancybox" rel="gallery-3" href="/img/index/review-image-big-1-1.jpg"><img src="/img/index/review-image-1-1.jpg" alt=""></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="wrapper scr-viii__wrapper bg-wrapper-3">
    <div class="inner scr-viii">
      <span class="scr-viii__title">КАК ПОЛУЧИТЬ ЛУЧШИЙ ТЕПЛЫЙ ПОЛ?</span>
      <div class="steps">
        <span class="step">Отправить заявку</span>
        <span class="step">Получить консультацию</span>
        <span class="step">Заказать</span>
        <span class="step">Наслаждаться теплом</span>
      </div>
    </div>
  </div>
  <footer class="footer">
    <div class="wrapper footer__1">
      <div class="inner">
        <button class="footer__btn" id="footer-button" data-init="modal" data-modal="#callback-modal" data-title="ЗАКАЗАТЬ ОБРАТНЫЙ ЗВОНОК" data-order="Заказ обратного звонка" data-hidden="/footer-callback.html">ЗАКАЗАТЬ ОБРАТНЫЙ ЗВОНОК</button>
        <div class="footer__logo">
          <img src="/img/index/footer-logo.png" alt="">
        </div>
        <span class="footer__phone">097-856-65-66</span>
      </div>
    </div>
    <div class="wrapper footer__2">
      <div class="inner">
        <a href="http://imsmedia.net.ua/" target="_blank" class="footer__copy">IMS MEDIA &copy; 2016</a>
      </div>
    </div>
  </footer>
  <div class="modal" id="callback-modal" data-type="modal">
    <div class="modal__wrapper">
    <span class="modal__close"></span>
      <div class="modal__inner">
        <form action="sendmessage.php" class="modal__form">
          <span class="modal__title">Заказать обратный звонок</span>
          <input type="hidden" name="order">
          <div class="modal__input-wrapper">
            <label for="phone" class="modal__label">Введите номер телефона</label>
            <input type="text" class="modal__input" name="phone" placeholder="+38 (0__) ___-__-__" id="phone">
          </div>
          <div class="modal__input-wrapper">
            <label for="name" class="modal__label">Введите имя</label>
            <input type="text" class="modal__input" name="name" placeholder="Имя" id="name">
          </div>
          <button class="modal__btn" type="submit" id="modal-button">отправить</button>
        </form>
      </div>
    </div>
  </div>
  <script src="js/index.js"></script>
</body>

</html>

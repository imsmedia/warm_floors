// jQuery.validator.setDefaults({
//   debug: true,
//   success: "valid"
// });
$.validator.addMethod('uaphone', function(value, element, params) {
  var regex = /\+38 \(0[0-9]{2}\) [0-9]{3}-[0-9]{2}-[0-9]{2}/g;
  var match = value.match(regex);
  if (match !== null) {
    if (match.length === 1) return true;
  }
  return false;
});
dataLayer = [];
$(document).ready(function() {
  'use strict';
  $('[name="phone"]').mask('+38 (999) 999-99-99');
  var forms = document.getElementsByTagName('form');
  for (var i = 0; i < forms.length; i++) {
    var form = forms[i];
    $(form).validate({
      rules: {
        phone: {
          required: true,
          uaphone: true
        }
      },
      messages: {
        phone: {
          required: 'Введите номер телефона',
          uaphone: 'Введите корректный номер'
        }
      },
      submitHandler: formSubmit
    });
  };
  function formSubmit(form, e) {
    e.preventDefault();
    var text = $(form).find('button').html();
    var page = $(form).find('[name="tagmanager"]').val();
    $(form).find('[name="tagmanager"]').remove();
    var data = $(form).serialize();
    $.ajax({
      url: 'sendmessage.php',
      type: 'POST',
      data: data,
      beforeSend: function(){
        $(form).find('input, button').attr('disabled', '');
        $(form).find('button').html('Отправляем...');
      }
    })
    .done(function(response) {
      // $(form).find('input, button').removeAttr('disabled');
      // $(form).find('input').val('');
      $(form).find('button').html('Отправлено!');
      dataLayer.push({
        'event' : 'VirtualPageview',
        'virtualPageURL' : page,
        'virtualPageTitle' : page.replace('/', '')
      });
    })
    .fail(function(response) {
      $(form).find('button').html('Не вышло :(');
    });
  };
  $('#tabs').tabs();
  $('#reviews').slick({
    nextArrow: '<button class="arrow--next" type="button"><span></span><span></span></button>',
    prevArrow: '<button class="arrow--prev" type="button"><span></span><span></span></button>'
  });
  $('.fancybox').fancybox({
    padding: 0,
    tpl: {
      closeBtn : '<span class="modal__close"></span>',
    },
    closeClick: true,
  });
  $('[data-init="modal"]').on('click', function() {
    var selector = $(this).attr('data-modal');
    var title = $(this).attr('data-title');
    var order = $(this).attr('data-order');
    var hiddenValue = $(this).attr('data-hidden');
    var modal = document.querySelector(selector);
    var form = modal.querySelector('form');
    var removed = form.querySelector('input[name="tagmanager"]');
    if (removed) removed.remove();
    var hidden = document.createElement('input');
    hidden.setAttribute('type', 'hidden');
    hidden.setAttribute('name', 'tagmanager');
    hidden.value = hiddenValue;
    form.appendChild(hidden);
    $(modal).addClass('active').find('.modal__title').html(title);
    $(modal).find('[name="order"]').val(order);
    setTimeout(function(){
      $(modal).find('[name="phone"]')[0].focus();
    }, 100);
  });
  $('.modal__close').on('click', function() {
    $(this).parent().parent().removeClass('active');
  });
  $('[data-type="modal"]').on('click', function() {
    $(this).removeClass('active');
  });
  $('.modal__wrapper').on('click', function(event) {
    event.stopPropagation();
  });
});

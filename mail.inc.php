<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Новая заявка на сайте %s</title>
  <style type="text/css">
			#outlook a {padding:0;}
			body{width:100%% !important; -webkit-text-size-adjust:100%%; -ms-text-size-adjust:100%%; margin:0; padding:0;}
			.ExternalClass {width:100%%;}
			.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%%;}
			img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
			a img {border:none;}
			p {margin:0 0 1em 0;}
			table td {border-collapse: collapse;}
			table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
		</style>
</head>
<body style="margin:0;">
  <table cellpadding="0" cellspacing="0"  border="0" width="100%%" height="100%%" style="background-image: url('http://evth.imsmedia.in.ua/mailassets/back.jpg');background-repeat: repeat;">
    <tr>
      <td height="55"></td>
    </tr>
    <tr>
      <td>
        <table cellpadding="0" cellspacing="0" border="0" width="600" align="center" style="background: #fff">
          <tr>
            <td style="padding: 30px 10px; font-family:Arial, sans-serif; font-size: 20px; background-color:#ae0000; color: #ffffff" align="center">Новая заявка на сайте %s</td>
          </tr>
          <tr>
            <td style="padding: 10px; font-family: Arial, sans-serif; font-size: 16px;">
              Отреагируйте на заявку как можно быстрее!
            </td>
          </tr>
          <tr>
            <td style="padding: 10px; font-family: Arial, sans-serif; font-size: 16px;">
              Полученные данные
            </td>
          </tr>
          <tr>
            <td>
              <table cellpadding="0" cellspacing="0" border="1" width="580" align="center">
                <tr>
                  <td width="200" style="padding: 3px 3px 3px 10px; font-family: Arial, sans-serif; font-size: 14px; font-weight:bold">Телефон</td>
                  <td width="400" style="padding: 3px; font-family: Arial, sans-serif; font-size: 14px">%s</td>
                </tr>
                <tr>
                  <td width="200" style="padding: 3px 3px 3px 10px; font-family: Arial, sans-serif; font-size: 14px; font-weight:bold">Имя</td>
                  <td width="400" style="padding: 3px; font-family: Arial, sans-serif; font-size: 14px">%s</td>
                </tr>
                <tr>
                  <td width="200" style="padding: 3px 3px 3px 10px; font-family: Arial, sans-serif; font-size: 14px; font-weight:bold">Email</td>
                  <td width="400" style="padding: 3px; font-family: Arial, sans-serif; font-size: 14px">%s</td>
                </tr>
                <tr>
                  <td width="600" colspan="2" style="padding: 3px 3px 3px 10px; font-family: Arial, sans-serif; font-size: 14px; text-align: center;">Информация о переходе</td>
                </tr>
                <tr>
                  <td width="200" style="padding: 3px 3px 3px 10px; font-family: Arial, sans-serif; font-size: 14px; font-weight:bold">Заявка пришла со страницы:</td>
                  <td width="400" style="padding: 3px; font-family: Arial, sans-serif; font-size: 14px">%s</td>
                </tr>
                <tr>
                  <td width="200" style="padding: 3px 3px 3px 10px; font-family: Arial, sans-serif; font-size: 14px; font-weight:bold">Поисковая система:</td>
                  <td width="400" style="padding: 3px; font-family: Arial, sans-serif; font-size: 14px">%s</td>
                </tr>
                <tr>
                  <td width="200" style="padding: 3px 3px 3px 10px; font-family: Arial, sans-serif; font-size: 14px; font-weight:bold">Ключ:</td>
                  <td width="400" style="padding: 3px; font-family: Arial, sans-serif; font-size: 14px">%s</td>
                </tr>
                <tr>
                  <td width="200" style="padding: 3px 3px 3px 10px; font-family: Arial, sans-serif; font-size: 14px; font-weight:bold">Кампания:</td>
                  <td width="400" style="padding: 3px; font-family: Arial, sans-serif; font-size: 14px">%s</td>
                </tr>
                <tr>
                  <td width="200" style="padding: 3px 3px 3px 10px; font-family: Arial, sans-serif; font-size: 14px; font-weight:bold">ip-адрес отправителя:</td>
                  <td width="400" style="padding: 3px; font-family: Arial, sans-serif; font-size: 14px">%s</td>
                </tr>
                <tr>
                  <td width="600" colspan="2" style="padding: 3px 3px 3px 10px;"></td>
                </tr>
                <tr>
                  <td width="200" style="background-color:#ffd3c7;padding: 3px 3px 3px 10px; font-family: Arial, sans-serif; font-size: 14px; font-weight:bold">Заявка составлена с</td>
                  <td width="400" style="background-color:#ffd3c7;padding: 3px; font-family: Arial, sans-serif; font-size: 14px">%s</td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td height="55"></td>
          </tr>
          <tr>
            <td>
              <table cellpadding="0" cellspacing="0" border="0" width="100%%" style="border-top: 3px solid #bf0000; border-bottom: 3px solid #bf0000;">
                <tr>
                  <td style="padding:10px 0 0" width="100" align="center"><img src="http://evth.imsmedia.in.ua/mailassets/ims-logo.png" alt="" /></td>
                  <td width="480" style="font-family: Arial, sans-serif; font-size: 14px; color: #5E5E5E; padding:10px 10px 0">+38 097 088-00-29<br>+38 066 830-47-55<br><a href="mailto:info@imsmedia.net.ua" style="font-family: Arial, sans-serif; font-size: 14px; color: #5E5E5E">info@imsmedia.net.ua</a></td>
                </tr>
                <tr>
                  <td height="15"></td>
                  <td height="15"></td>

                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td height="55"></td>
    </tr>
  </table>
</body>
</html>
